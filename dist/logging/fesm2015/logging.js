import { HttpHeaders, HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { utc } from 'moment';
import { Injectable, defineInjectable, inject } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LogEntry {
    /**
     * @param {?} message
     * @param {?} level
     * @param {?} extraInfo
     * @param {?} logWithDate
     */
    constructor(message, level, extraInfo, logWithDate) {
        this.message = message;
        this.level = level;
        this.extraInfo = extraInfo;
        this.logWithDate = logWithDate;
    }
    /**
     * @return {?}
     */
    buildLogString() {
        let /** @type {?} */ value = '';
        if (this.logWithDate) {
            value += utc().format('YYYY MMM D (z), h:mm:ss:SSS ');
        }
        value += 'Type: ' + LogLevel[this.level];
        value += ' - Message: ' + this.message;
        if (this.extraInfo.length) {
            value += ' - Extra Info: '
                + this.formatParams(this.extraInfo);
        }
        return value;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    formatParams(params) {
        let /** @type {?} */ ret = params.join(',');
        // Is there at least one object in the array?
        if (params.some(p => typeof p === 'object')) {
            ret = '';
            // Build comma-delimited string
            for (const /** @type {?} */ item of params) {
                ret += JSON.stringify(item) + ',';
            }
        }
        return ret;
    }
}
/** @enum {number} */
const LogLevel = {
    ALL: 0,
    DEBUG: 1,
    INFO: 2,
    WARN: 3,
    ERROR: 4,
    FATAL: 5,
    OFF: 6,
};
LogLevel[LogLevel.ALL] = "ALL";
LogLevel[LogLevel.DEBUG] = "DEBUG";
LogLevel[LogLevel.INFO] = "INFO";
LogLevel[LogLevel.WARN] = "WARN";
LogLevel[LogLevel.ERROR] = "ERROR";
LogLevel[LogLevel.FATAL] = "FATAL";
LogLevel[LogLevel.OFF] = "OFF";
/**
 * @abstract
 */
class LogPublisher {
}
class LogConsole extends LogPublisher {
    /**
     * @param {?} entry
     * @return {?}
     */
    log(entry) {
        switch (entry.level) {
            case LogLevel.ALL:
                console.log(entry.buildLogString());
                break;
            case LogLevel.DEBUG:
                // tslint:disable-next-line:no-console
                console.log(entry.buildLogString());
                break;
            case LogLevel.INFO:
                // tslint:disable-next-line:no-console
                console.info(entry.buildLogString());
                break;
            case LogLevel.WARN:
                console.warn(entry.buildLogString());
                break;
            case LogLevel.FATAL:
                console.error(entry.buildLogString());
                break;
            case LogLevel.ERROR:
                console.error(entry.buildLogString());
                break;
            default:
                console.log(entry.buildLogString());
                break;
        }
        return of(true);
    }
    /**
     * @return {?}
     */
    clear() {
        console.clear();
        return of(true);
    }
}
const /** @type {?} */ httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
class LogWebApi extends LogPublisher {
    /**
     * @param {?} http
     * @param {?} logUrl
     */
    constructor(http, logUrl) {
        super();
        this.http = http;
        this.logUrl = logUrl;
        this.http = http;
        this.logUrl = logUrl;
    }
    /**
     * @param {?} entry
     * @return {?}
     */
    log(entry) {
        return this.http.post(this.logUrl, entry, httpOptions)
            .pipe(catchError(this.handleError('post log')));
    }
    /**
     * @return {?}
     */
    clear() {
        // TODO: Call Web API to clear all values
        return of(true);
    }
    /**
     * @param {?} message
     * @return {?}
     */
    handleError(message) {
        return (error) => {
            console.error(error);
            return of(message);
        };
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LoggingService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.logWithDate = true;
        this.level = LogLevel.ALL;
        this.logPublishers = [];
        this.logPublishers.push(new LogConsole());
        this.logPublishers.push(new LogWebApi(httpClient, 'http://localhost:58341/api/logging'));
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    debug(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.DEBUG, optionalParams);
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    info(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.INFO, optionalParams);
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    warn(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.WARN, optionalParams);
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    error(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.ERROR, optionalParams);
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    fatal(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.FATAL, optionalParams);
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    log(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.ALL, optionalParams);
    }
    /**
     * @return {?}
     */
    clear() {
        for (const /** @type {?} */ logger of this.logPublishers) {
            logger.clear();
        }
    }
    /**
     * @param {?} msg
     * @param {?} level
     * @param {?} params
     * @return {?}
     */
    writeToLog(msg, level, params) {
        if (this.shouldLog(level)) {
            const /** @type {?} */ entry = new LogEntry(msg, level, params, this.logWithDate);
            for (const /** @type {?} */ logger of this.logPublishers) {
                logger.log(entry);
            }
        }
    }
    /**
     * @param {?} level
     * @return {?}
     */
    shouldLog(level) {
        let /** @type {?} */ ret = false;
        if ((level >= this.level &&
            level !== LogLevel.OFF) ||
            this.level === LogLevel.ALL) {
            ret = true;
        }
        return ret;
    }
}
LoggingService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
LoggingService.ctorParameters = () => [
    { type: HttpClient, },
];
/** @nocollapse */ LoggingService.ngInjectableDef = defineInjectable({ factory: function LoggingService_Factory() { return new LoggingService(inject(HttpClient)); }, token: LoggingService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { LoggingService };
//# sourceMappingURL=logging.js.map
