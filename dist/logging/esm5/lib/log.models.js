import * as tslib_1 from "tslib";
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { utc } from 'moment';
var LogEntry = /** @class */ (function () {
    /**
     * @param {?} message
     * @param {?} level
     * @param {?} extraInfo
     * @param {?} logWithDate
     */
    function LogEntry(message, level, extraInfo, logWithDate) {
        this.message = message;
        this.level = level;
        this.extraInfo = extraInfo;
        this.logWithDate = logWithDate;
    }
    /**
     * @return {?}
     */
    LogEntry.prototype.buildLogString = function () {
        var /** @type {?} */ value = '';
        if (this.logWithDate) {
            value += utc().format('YYYY MMM D (z), h:mm:ss:SSS ');
        }
        value += 'Type: ' + LogLevel[this.level];
        value += ' - Message: ' + this.message;
        if (this.extraInfo.length) {
            value += ' - Extra Info: '
                + this.formatParams(this.extraInfo);
        }
        return value;
    };
    /**
     * @param {?} params
     * @return {?}
     */
    LogEntry.prototype.formatParams = function (params) {
        var /** @type {?} */ ret = params.join(',');
        // Is there at least one object in the array?
        if (params.some(function (p) { return typeof p === 'object'; })) {
            ret = '';
            try {
                // Build comma-delimited string
                for (var params_1 = tslib_1.__values(params), params_1_1 = params_1.next(); !params_1_1.done; params_1_1 = params_1.next()) {
                    var item = params_1_1.value;
                    ret += JSON.stringify(item) + ',';
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (params_1_1 && !params_1_1.done && (_a = params_1.return)) _a.call(params_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        return ret;
        var e_1, _a;
    };
    return LogEntry;
}());
export { LogEntry };
function LogEntry_tsickle_Closure_declarations() {
    /** @type {?} */
    LogEntry.prototype.logWithDate;
    /** @type {?} */
    LogEntry.prototype.extraInfo;
    /** @type {?} */
    LogEntry.prototype.message;
    /** @type {?} */
    LogEntry.prototype.level;
}
/** @enum {number} */
var LogLevel = {
    ALL: 0,
    DEBUG: 1,
    INFO: 2,
    WARN: 3,
    ERROR: 4,
    FATAL: 5,
    OFF: 6,
};
export { LogLevel };
LogLevel[LogLevel.ALL] = "ALL";
LogLevel[LogLevel.DEBUG] = "DEBUG";
LogLevel[LogLevel.INFO] = "INFO";
LogLevel[LogLevel.WARN] = "WARN";
LogLevel[LogLevel.ERROR] = "ERROR";
LogLevel[LogLevel.FATAL] = "FATAL";
LogLevel[LogLevel.OFF] = "OFF";
/**
 * @abstract
 */
var LogPublisher = /** @class */ (function () {
    function LogPublisher() {
    }
    return LogPublisher;
}());
export { LogPublisher };
function LogPublisher_tsickle_Closure_declarations() {
    /** @type {?} */
    LogPublisher.prototype.location;
    /**
     * @abstract
     * @param {?} record
     * @return {?}
     */
    LogPublisher.prototype.log = function (record) { };
    /**
     * @abstract
     * @return {?}
     */
    LogPublisher.prototype.clear = function () { };
}
var LogConsole = /** @class */ (function (_super) {
    tslib_1.__extends(LogConsole, _super);
    function LogConsole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @param {?} entry
     * @return {?}
     */
    LogConsole.prototype.log = function (entry) {
        switch (entry.level) {
            case LogLevel.ALL:
                console.log(entry.buildLogString());
                break;
            case LogLevel.DEBUG:
                // tslint:disable-next-line:no-console
                console.log(entry.buildLogString());
                break;
            case LogLevel.INFO:
                // tslint:disable-next-line:no-console
                console.info(entry.buildLogString());
                break;
            case LogLevel.WARN:
                console.warn(entry.buildLogString());
                break;
            case LogLevel.FATAL:
                console.error(entry.buildLogString());
                break;
            case LogLevel.ERROR:
                console.error(entry.buildLogString());
                break;
            default:
                console.log(entry.buildLogString());
                break;
        }
        return of(true);
    };
    /**
     * @return {?}
     */
    LogConsole.prototype.clear = function () {
        console.clear();
        return of(true);
    };
    return LogConsole;
}(LogPublisher));
export { LogConsole };
var /** @type {?} */ httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
var LogWebApi = /** @class */ (function (_super) {
    tslib_1.__extends(LogWebApi, _super);
    /**
     * @param {?} http
     * @param {?} logUrl
     */
    function LogWebApi(http, logUrl) {
        var _this = _super.call(this) || this;
        _this.http = http;
        _this.logUrl = logUrl;
        _this.http = http;
        _this.logUrl = logUrl;
        return _this;
    }
    /**
     * @param {?} entry
     * @return {?}
     */
    LogWebApi.prototype.log = function (entry) {
        return this.http.post(this.logUrl, entry, httpOptions)
            .pipe(catchError(this.handleError('post log')));
    };
    /**
     * @return {?}
     */
    LogWebApi.prototype.clear = function () {
        // TODO: Call Web API to clear all values
        return of(true);
    };
    /**
     * @param {?} message
     * @return {?}
     */
    LogWebApi.prototype.handleError = function (message) {
        return function (error) {
            console.error(error);
            return of(message);
        };
    };
    return LogWebApi;
}(LogPublisher));
export { LogWebApi };
function LogWebApi_tsickle_Closure_declarations() {
    /** @type {?} */
    LogWebApi.prototype.http;
    /** @type {?} */
    LogWebApi.prototype.logUrl;
}
//# sourceMappingURL=log.models.js.map
