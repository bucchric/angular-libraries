import * as tslib_1 from "tslib";
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LogLevel, LogConsole, LogEntry, LogWebApi } from './log.models';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var LoggingService = /** @class */ (function () {
    /**
     * @param {?} httpClient
     */
    function LoggingService(httpClient) {
        this.httpClient = httpClient;
        this.logWithDate = true;
        this.level = LogLevel.ALL;
        this.logPublishers = [];
        this.logPublishers.push(new LogConsole());
        this.logPublishers.push(new LogWebApi(httpClient, 'http://localhost:58341/api/logging'));
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.debug = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.DEBUG, optionalParams);
    };
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.info = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.INFO, optionalParams);
    };
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.warn = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.WARN, optionalParams);
    };
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.error = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.ERROR, optionalParams);
    };
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.fatal = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.FATAL, optionalParams);
    };
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.log = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.ALL, optionalParams);
    };
    /**
     * @return {?}
     */
    LoggingService.prototype.clear = function () {
        try {
            for (var _a = tslib_1.__values(this.logPublishers), _b = _a.next(); !_b.done; _b = _a.next()) {
                var logger = _b.value;
                logger.clear();
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        var e_1, _c;
    };
    /**
     * @param {?} msg
     * @param {?} level
     * @param {?} params
     * @return {?}
     */
    LoggingService.prototype.writeToLog = function (msg, level, params) {
        if (this.shouldLog(level)) {
            var /** @type {?} */ entry = new LogEntry(msg, level, params, this.logWithDate);
            try {
                for (var _a = tslib_1.__values(this.logPublishers), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var logger = _b.value;
                    logger.log(entry);
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        var e_2, _c;
    };
    /**
     * @param {?} level
     * @return {?}
     */
    LoggingService.prototype.shouldLog = function (level) {
        var /** @type {?} */ ret = false;
        if ((level >= this.level &&
            level !== LogLevel.OFF) ||
            this.level === LogLevel.ALL) {
            ret = true;
        }
        return ret;
    };
    return LoggingService;
}());
export { LoggingService };
LoggingService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
LoggingService.ctorParameters = function () { return [
    { type: HttpClient, },
]; };
/** @nocollapse */ LoggingService.ngInjectableDef = i0.defineInjectable({ factory: function LoggingService_Factory() { return new LoggingService(i0.inject(i1.HttpClient)); }, token: LoggingService, providedIn: "root" });
function LoggingService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    LoggingService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    LoggingService.ctorParameters;
    /** @type {?} */
    LoggingService.prototype.logWithDate;
    /** @type {?} */
    LoggingService.prototype.level;
    /** @type {?} */
    LoggingService.prototype.logPublishers;
    /** @type {?} */
    LoggingService.prototype.httpClient;
}
//# sourceMappingURL=logging.service.js.map
