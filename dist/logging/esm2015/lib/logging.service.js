/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LogLevel, LogConsole, LogEntry, LogWebApi } from './log.models';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class LoggingService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.logWithDate = true;
        this.level = LogLevel.ALL;
        this.logPublishers = [];
        this.logPublishers.push(new LogConsole());
        this.logPublishers.push(new LogWebApi(httpClient, 'http://localhost:58341/api/logging'));
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    debug(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.DEBUG, optionalParams);
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    info(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.INFO, optionalParams);
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    warn(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.WARN, optionalParams);
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    error(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.ERROR, optionalParams);
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    fatal(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.FATAL, optionalParams);
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    log(msg, ...optionalParams) {
        this.writeToLog(msg, LogLevel.ALL, optionalParams);
    }
    /**
     * @return {?}
     */
    clear() {
        for (const /** @type {?} */ logger of this.logPublishers) {
            logger.clear();
        }
    }
    /**
     * @param {?} msg
     * @param {?} level
     * @param {?} params
     * @return {?}
     */
    writeToLog(msg, level, params) {
        if (this.shouldLog(level)) {
            const /** @type {?} */ entry = new LogEntry(msg, level, params, this.logWithDate);
            for (const /** @type {?} */ logger of this.logPublishers) {
                logger.log(entry);
            }
        }
    }
    /**
     * @param {?} level
     * @return {?}
     */
    shouldLog(level) {
        let /** @type {?} */ ret = false;
        if ((level >= this.level &&
            level !== LogLevel.OFF) ||
            this.level === LogLevel.ALL) {
            ret = true;
        }
        return ret;
    }
}
LoggingService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
LoggingService.ctorParameters = () => [
    { type: HttpClient, },
];
/** @nocollapse */ LoggingService.ngInjectableDef = i0.defineInjectable({ factory: function LoggingService_Factory() { return new LoggingService(i0.inject(i1.HttpClient)); }, token: LoggingService, providedIn: "root" });
function LoggingService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    LoggingService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    LoggingService.ctorParameters;
    /** @type {?} */
    LoggingService.prototype.logWithDate;
    /** @type {?} */
    LoggingService.prototype.level;
    /** @type {?} */
    LoggingService.prototype.logPublishers;
    /** @type {?} */
    LoggingService.prototype.httpClient;
}
//# sourceMappingURL=logging.service.js.map
