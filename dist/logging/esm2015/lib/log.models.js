/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { utc } from 'moment';
export class LogEntry {
    /**
     * @param {?} message
     * @param {?} level
     * @param {?} extraInfo
     * @param {?} logWithDate
     */
    constructor(message, level, extraInfo, logWithDate) {
        this.message = message;
        this.level = level;
        this.extraInfo = extraInfo;
        this.logWithDate = logWithDate;
    }
    /**
     * @return {?}
     */
    buildLogString() {
        let /** @type {?} */ value = '';
        if (this.logWithDate) {
            value += utc().format('YYYY MMM D (z), h:mm:ss:SSS ');
        }
        value += 'Type: ' + LogLevel[this.level];
        value += ' - Message: ' + this.message;
        if (this.extraInfo.length) {
            value += ' - Extra Info: '
                + this.formatParams(this.extraInfo);
        }
        return value;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    formatParams(params) {
        let /** @type {?} */ ret = params.join(',');
        // Is there at least one object in the array?
        if (params.some(p => typeof p === 'object')) {
            ret = '';
            // Build comma-delimited string
            for (const /** @type {?} */ item of params) {
                ret += JSON.stringify(item) + ',';
            }
        }
        return ret;
    }
}
function LogEntry_tsickle_Closure_declarations() {
    /** @type {?} */
    LogEntry.prototype.logWithDate;
    /** @type {?} */
    LogEntry.prototype.extraInfo;
    /** @type {?} */
    LogEntry.prototype.message;
    /** @type {?} */
    LogEntry.prototype.level;
}
/** @enum {number} */
const LogLevel = {
    ALL: 0,
    DEBUG: 1,
    INFO: 2,
    WARN: 3,
    ERROR: 4,
    FATAL: 5,
    OFF: 6,
};
export { LogLevel };
LogLevel[LogLevel.ALL] = "ALL";
LogLevel[LogLevel.DEBUG] = "DEBUG";
LogLevel[LogLevel.INFO] = "INFO";
LogLevel[LogLevel.WARN] = "WARN";
LogLevel[LogLevel.ERROR] = "ERROR";
LogLevel[LogLevel.FATAL] = "FATAL";
LogLevel[LogLevel.OFF] = "OFF";
/**
 * @abstract
 */
export class LogPublisher {
}
function LogPublisher_tsickle_Closure_declarations() {
    /** @type {?} */
    LogPublisher.prototype.location;
    /**
     * @abstract
     * @param {?} record
     * @return {?}
     */
    LogPublisher.prototype.log = function (record) { };
    /**
     * @abstract
     * @return {?}
     */
    LogPublisher.prototype.clear = function () { };
}
export class LogConsole extends LogPublisher {
    /**
     * @param {?} entry
     * @return {?}
     */
    log(entry) {
        switch (entry.level) {
            case LogLevel.ALL:
                console.log(entry.buildLogString());
                break;
            case LogLevel.DEBUG:
                // tslint:disable-next-line:no-console
                console.log(entry.buildLogString());
                break;
            case LogLevel.INFO:
                // tslint:disable-next-line:no-console
                console.info(entry.buildLogString());
                break;
            case LogLevel.WARN:
                console.warn(entry.buildLogString());
                break;
            case LogLevel.FATAL:
                console.error(entry.buildLogString());
                break;
            case LogLevel.ERROR:
                console.error(entry.buildLogString());
                break;
            default:
                console.log(entry.buildLogString());
                break;
        }
        return of(true);
    }
    /**
     * @return {?}
     */
    clear() {
        console.clear();
        return of(true);
    }
}
const /** @type {?} */ httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
export class LogWebApi extends LogPublisher {
    /**
     * @param {?} http
     * @param {?} logUrl
     */
    constructor(http, logUrl) {
        super();
        this.http = http;
        this.logUrl = logUrl;
        this.http = http;
        this.logUrl = logUrl;
    }
    /**
     * @param {?} entry
     * @return {?}
     */
    log(entry) {
        return this.http.post(this.logUrl, entry, httpOptions)
            .pipe(catchError(this.handleError('post log')));
    }
    /**
     * @return {?}
     */
    clear() {
        // TODO: Call Web API to clear all values
        return of(true);
    }
    /**
     * @param {?} message
     * @return {?}
     */
    handleError(message) {
        return (error) => {
            console.error(error);
            return of(message);
        };
    }
}
function LogWebApi_tsickle_Closure_declarations() {
    /** @type {?} */
    LogWebApi.prototype.http;
    /** @type {?} */
    LogWebApi.prototype.logUrl;
}
//# sourceMappingURL=log.models.js.map
