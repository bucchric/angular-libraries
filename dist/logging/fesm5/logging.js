import { __values, __extends } from 'tslib';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { utc } from 'moment';
import { Injectable, defineInjectable, inject } from '@angular/core';

var LogEntry = /** @class */ (function () {
    /**
     * @param {?} message
     * @param {?} level
     * @param {?} extraInfo
     * @param {?} logWithDate
     */
    function LogEntry(message, level, extraInfo, logWithDate) {
        this.message = message;
        this.level = level;
        this.extraInfo = extraInfo;
        this.logWithDate = logWithDate;
    }
    /**
     * @return {?}
     */
    LogEntry.prototype.buildLogString = function () {
        var /** @type {?} */ value = '';
        if (this.logWithDate) {
            value += utc().format('YYYY MMM D (z), h:mm:ss:SSS ');
        }
        value += 'Type: ' + LogLevel[this.level];
        value += ' - Message: ' + this.message;
        if (this.extraInfo.length) {
            value += ' - Extra Info: '
                + this.formatParams(this.extraInfo);
        }
        return value;
    };
    /**
     * @param {?} params
     * @return {?}
     */
    LogEntry.prototype.formatParams = function (params) {
        var /** @type {?} */ ret = params.join(',');
        // Is there at least one object in the array?
        if (params.some(function (p) { return typeof p === 'object'; })) {
            ret = '';
            try {
                // Build comma-delimited string
                for (var params_1 = __values(params), params_1_1 = params_1.next(); !params_1_1.done; params_1_1 = params_1.next()) {
                    var item = params_1_1.value;
                    ret += JSON.stringify(item) + ',';
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (params_1_1 && !params_1_1.done && (_a = params_1.return)) _a.call(params_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        return ret;
        var e_1, _a;
    };
    return LogEntry;
}());
/** @enum {number} */
var LogLevel = {
    ALL: 0,
    DEBUG: 1,
    INFO: 2,
    WARN: 3,
    ERROR: 4,
    FATAL: 5,
    OFF: 6,
};
LogLevel[LogLevel.ALL] = "ALL";
LogLevel[LogLevel.DEBUG] = "DEBUG";
LogLevel[LogLevel.INFO] = "INFO";
LogLevel[LogLevel.WARN] = "WARN";
LogLevel[LogLevel.ERROR] = "ERROR";
LogLevel[LogLevel.FATAL] = "FATAL";
LogLevel[LogLevel.OFF] = "OFF";
/**
 * @abstract
 */
var LogPublisher = /** @class */ (function () {
    function LogPublisher() {
    }
    return LogPublisher;
}());
var LogConsole = /** @class */ (function (_super) {
    __extends(LogConsole, _super);
    function LogConsole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @param {?} entry
     * @return {?}
     */
    LogConsole.prototype.log = function (entry) {
        switch (entry.level) {
            case LogLevel.ALL:
                console.log(entry.buildLogString());
                break;
            case LogLevel.DEBUG:
                // tslint:disable-next-line:no-console
                console.log(entry.buildLogString());
                break;
            case LogLevel.INFO:
                // tslint:disable-next-line:no-console
                console.info(entry.buildLogString());
                break;
            case LogLevel.WARN:
                console.warn(entry.buildLogString());
                break;
            case LogLevel.FATAL:
                console.error(entry.buildLogString());
                break;
            case LogLevel.ERROR:
                console.error(entry.buildLogString());
                break;
            default:
                console.log(entry.buildLogString());
                break;
        }
        return of(true);
    };
    /**
     * @return {?}
     */
    LogConsole.prototype.clear = function () {
        console.clear();
        return of(true);
    };
    return LogConsole;
}(LogPublisher));
var /** @type {?} */ httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
var LogWebApi = /** @class */ (function (_super) {
    __extends(LogWebApi, _super);
    /**
     * @param {?} http
     * @param {?} logUrl
     */
    function LogWebApi(http, logUrl) {
        var _this = _super.call(this) || this;
        _this.http = http;
        _this.logUrl = logUrl;
        _this.http = http;
        _this.logUrl = logUrl;
        return _this;
    }
    /**
     * @param {?} entry
     * @return {?}
     */
    LogWebApi.prototype.log = function (entry) {
        return this.http.post(this.logUrl, entry, httpOptions)
            .pipe(catchError(this.handleError('post log')));
    };
    /**
     * @return {?}
     */
    LogWebApi.prototype.clear = function () {
        // TODO: Call Web API to clear all values
        return of(true);
    };
    /**
     * @param {?} message
     * @return {?}
     */
    LogWebApi.prototype.handleError = function (message) {
        return function (error) {
            console.error(error);
            return of(message);
        };
    };
    return LogWebApi;
}(LogPublisher));

var LoggingService = /** @class */ (function () {
    /**
     * @param {?} httpClient
     */
    function LoggingService(httpClient) {
        this.httpClient = httpClient;
        this.logWithDate = true;
        this.level = LogLevel.ALL;
        this.logPublishers = [];
        this.logPublishers.push(new LogConsole());
        this.logPublishers.push(new LogWebApi(httpClient, 'http://localhost:58341/api/logging'));
    }
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.debug = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.DEBUG, optionalParams);
    };
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.info = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.INFO, optionalParams);
    };
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.warn = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.WARN, optionalParams);
    };
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.error = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.ERROR, optionalParams);
    };
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.fatal = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.FATAL, optionalParams);
    };
    /**
     * @param {?} msg
     * @param {...?} optionalParams
     * @return {?}
     */
    LoggingService.prototype.log = function (msg) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        this.writeToLog(msg, LogLevel.ALL, optionalParams);
    };
    /**
     * @return {?}
     */
    LoggingService.prototype.clear = function () {
        try {
            for (var _a = __values(this.logPublishers), _b = _a.next(); !_b.done; _b = _a.next()) {
                var logger = _b.value;
                logger.clear();
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        var e_1, _c;
    };
    /**
     * @param {?} msg
     * @param {?} level
     * @param {?} params
     * @return {?}
     */
    LoggingService.prototype.writeToLog = function (msg, level, params) {
        if (this.shouldLog(level)) {
            var /** @type {?} */ entry = new LogEntry(msg, level, params, this.logWithDate);
            try {
                for (var _a = __values(this.logPublishers), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var logger = _b.value;
                    logger.log(entry);
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        var e_2, _c;
    };
    /**
     * @param {?} level
     * @return {?}
     */
    LoggingService.prototype.shouldLog = function (level) {
        var /** @type {?} */ ret = false;
        if ((level >= this.level &&
            level !== LogLevel.OFF) ||
            this.level === LogLevel.ALL) {
            ret = true;
        }
        return ret;
    };
    return LoggingService;
}());
LoggingService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
LoggingService.ctorParameters = function () { return [
    { type: HttpClient, },
]; };
/** @nocollapse */ LoggingService.ngInjectableDef = defineInjectable({ factory: function LoggingService_Factory() { return new LoggingService(inject(HttpClient)); }, token: LoggingService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { LoggingService };
//# sourceMappingURL=logging.js.map
