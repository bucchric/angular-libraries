import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
export declare class LogEntry {
    private logWithDate;
    private extraInfo;
    private message;
    level: LogLevel;
    constructor(message: string, level: LogLevel, extraInfo: any[], logWithDate: boolean);
    buildLogString(): string;
    private formatParams(params);
}
export declare enum LogLevel {
    ALL = 0,
    DEBUG = 1,
    INFO = 2,
    WARN = 3,
    ERROR = 4,
    FATAL = 5,
    OFF = 6,
}
export declare abstract class LogPublisher {
    location: string;
    abstract log(record: LogEntry): Observable<boolean>;
    abstract clear(): Observable<boolean>;
}
export declare class LogConsole extends LogPublisher {
    log(entry: LogEntry): Observable<boolean>;
    clear(): Observable<boolean>;
}
export declare class LogWebApi extends LogPublisher {
    private http;
    private logUrl;
    constructor(http: HttpClient, logUrl: string);
    log(entry: LogEntry): Observable<boolean>;
    clear(): Observable<boolean>;
    private handleError(message);
}
