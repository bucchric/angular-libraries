import { HttpClient } from '@angular/common/http';
export declare class LoggingService {
    private httpClient;
    private logWithDate;
    private level;
    private logPublishers;
    constructor(httpClient: HttpClient);
    debug(msg: string, ...optionalParams: any[]): void;
    info(msg: string, ...optionalParams: any[]): void;
    warn(msg: string, ...optionalParams: any[]): void;
    error(msg: string, ...optionalParams: any[]): void;
    fatal(msg: string, ...optionalParams: any[]): void;
    log(msg: string, ...optionalParams: any[]): void;
    clear(): void;
    private writeToLog(msg, level, params);
    private shouldLog(level);
}
