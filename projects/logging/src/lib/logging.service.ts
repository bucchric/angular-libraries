import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LogLevel, LogConsole, LogPublisher, LogEntry, LogWebApi } from './log.models';

@Injectable({
    providedIn: 'root'
})
export class LoggingService {

    private logWithDate: boolean;
    private level: LogLevel;
    private logPublishers: LogPublisher[];

    constructor(private httpClient: HttpClient) {
        this.logWithDate = true;
        this.level = LogLevel.ALL;
        this.logPublishers = [];
        this.logPublishers.push(new LogConsole());
        this.logPublishers.push(new LogWebApi(httpClient, 'http://localhost:58341/api/logging'));
    }

    public debug(msg: string, ...optionalParams: any[]) {
        this.writeToLog(msg, LogLevel.DEBUG,
            optionalParams);
    }

    public info(msg: string, ...optionalParams: any[]) {
        this.writeToLog(msg, LogLevel.INFO,
            optionalParams);
    }

    public warn(msg: string, ...optionalParams: any[]) {
        this.writeToLog(msg, LogLevel.WARN,
            optionalParams);
    }

    public error(msg: string, ...optionalParams: any[]) {
        this.writeToLog(msg, LogLevel.ERROR,
            optionalParams);
    }

    public fatal(msg: string, ...optionalParams: any[]) {
        this.writeToLog(msg, LogLevel.FATAL,
            optionalParams);
    }

    public log(msg: string, ...optionalParams: any[]) {
        this.writeToLog(msg, LogLevel.ALL,
            optionalParams);
    }

    public clear() {
        for (const logger of this.logPublishers) {
            logger.clear();
        }
    }

    private writeToLog(msg: string,
        level: LogLevel,
        params: any[]): void {
        if (this.shouldLog(level)) {
            const entry = new LogEntry(msg, level, params, this.logWithDate);

            for (const logger of this.logPublishers) {
                logger.log(entry);
            }
        }
    }

    private shouldLog(level: LogLevel): boolean {
        let ret = false;
        if ((level >= this.level &&
            level !== LogLevel.OFF) ||
            this.level === LogLevel.ALL) {
            ret = true;
        }
        return ret;
    }
}
