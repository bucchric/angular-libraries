import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { utc } from 'moment';

export class LogEntry {

    private logWithDate: boolean;
    private extraInfo: any[];
    private message: string;

    public level: LogLevel;

    constructor(message: string, level: LogLevel, extraInfo: any[], logWithDate: boolean) {
        this.message = message;
        this.level = level;
        this.extraInfo = extraInfo;
        this.logWithDate = logWithDate;
    }

    public buildLogString(): string {
        let value = '';

        if (this.logWithDate) {
            value += utc().format('YYYY MMM D (z), h:mm:ss:SSS ');
        }
        value += 'Type: ' + LogLevel[this.level];
        value += ' - Message: ' + this.message;
        if (this.extraInfo.length) {
            value += ' - Extra Info: '
                + this.formatParams(this.extraInfo);
        }
        return value;
    }

    private formatParams(params: any[]): string {
        let ret: string = params.join(',');
        // Is there at least one object in the array?
        if (params.some(p => typeof p === 'object')) {
            ret = '';
            // Build comma-delimited string
            for (const item of params) {
                ret += JSON.stringify(item) + ',';
            }
        }
        return ret;
    }
}

export enum LogLevel {
    ALL = 0,
    DEBUG = 1,
    INFO = 2,
    WARN = 3,
    ERROR = 4,
    FATAL = 5,
    OFF = 6
}

export abstract class LogPublisher {
    location: string;
    abstract log(record: LogEntry): Observable<boolean>;
    abstract clear(): Observable<boolean>;
}

export class LogConsole extends LogPublisher {

    log(entry: LogEntry): Observable<boolean> {
        switch (entry.level) {
            case LogLevel.ALL:
                console.log(entry.buildLogString());
                break;
            case LogLevel.DEBUG:
                // tslint:disable-next-line:no-console
                console.log(entry.buildLogString());
                break;
            case LogLevel.INFO:
                // tslint:disable-next-line:no-console
                console.info(entry.buildLogString());
                break;
            case LogLevel.WARN:
                console.warn(entry.buildLogString());
                break;
            case LogLevel.FATAL:
                console.error(entry.buildLogString());
                break;
            case LogLevel.ERROR:
                console.error(entry.buildLogString());
                break;
            default:
                console.log(entry.buildLogString());
                break;
        }
        return of(true);
    }

    clear(): Observable<boolean> {
        console.clear();
        return of(true);
    }
}

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
export class LogWebApi extends LogPublisher {

    constructor(private http: HttpClient, private logUrl: string) {
        super();
        this.http = http;
        this.logUrl = logUrl;
    }

    log(entry: LogEntry): Observable<boolean> {
        return this.http.post<LogEntry>(this.logUrl, entry, httpOptions)
            .pipe(catchError(this.handleError('post log')),
        );
    }

    // Clear all log entries from local storage
    clear(): Observable<boolean> {
        // TODO: Call Web API to clear all values
        return of(true);
    }

    private handleError(message: string) {
        return (error: any): Observable<any> => {
            console.error(error);
            return of(message);
        };
    }
}
