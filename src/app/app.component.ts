import { Component, OnInit } from '@angular/core';
import { LoggingService } from 'logging';
import { logging } from 'protractor';

@Component({
    selector: 'here-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    title = 'here';
    constructor(private loggingService: LoggingService) {

    }

    ngOnInit(): void {
        this.loggingService.info('info', { 'User': 'Riccardo', 'Id': 1 });
        this.loggingService.fatal('fatal');
        this.loggingService.error('error');
        this.loggingService.log('log');
        this.loggingService.warn('warn');
    }


}
